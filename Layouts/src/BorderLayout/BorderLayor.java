package BorderLayout;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class BorderLayor extends JFrame {

    private JPanel contentPane;
    private JLabel lblName;
    private JTextField textField;
    private JButton btnResetName;
    private JLabel lblAnmeldung;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    BorderLayor frame = new BorderLayor();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public BorderLayor() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));
        
        JButton btnBesttigen = new JButton("Best\u00E4tigen");
        btnBesttigen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                System.out.println("loading");
                for(int i = 0; i <3;i++) {
                    System.out.println(".");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                
                    System.out.println("Anmeldung Abgeschlossen");
                    System.exit(0);
            }
        });
    
        contentPane.add(btnBesttigen, BorderLayout.SOUTH);
        
        lblName = new JLabel("Name:");
        contentPane.add(lblName, BorderLayout.WEST);
        
        textField = new JTextField();
        contentPane.add(textField, BorderLayout.CENTER);
        textField.setColumns(10);
        
        btnResetName = new JButton("Reset Name");
        btnResetName.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                textField.setText("");
            }
        });
        contentPane.add(btnResetName, BorderLayout.EAST);
        
        lblAnmeldung = new JLabel("Anmeldung");
        lblAnmeldung.setHorizontalAlignment(SwingConstants.CENTER);
        contentPane.add(lblAnmeldung, BorderLayout.NORTH);
    }

}
