package FlowLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CookieClicker extends JFrame {

    private JPanel contentPane;
    private JLabel ccdl;
    int cc = 0;
    double cr = 0.0;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    CookieClicker frame = new CookieClicker();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public CookieClicker() {
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        
        JButton btn = new JButton("Higher");
        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                cc++;
                ccdl.setText("Cookies Anzahl: " + cc + " Cookies");
                if (cc > 100) {
                    ccdl.setText("reached End");
                    if (cc > 100){
                        cc = 101;
                    }
                }
            }
        });
        contentPane.add(btn);
        
        JButton btn1 = new JButton("Lower");
        btn1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cc--;
                if (cc < 0){
                    cc = 0;
                }
                ccdl.setText("Cookies Anzahl: " + cc + " Cookies");
            }
        });
        contentPane.add(btn1);
        
        ccdl = new JLabel("Cookie Anzahl: " + cc + " Cookies", JLabel.CENTER);
        getContentPane().add(ccdl);
        
        JButton btnBeenden = new JButton("Beenden");
        btnBeenden.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("Designed in Germany, Made in China");
                System.exit(0);
            }
        });
        contentPane.add(btnBeenden);
    }
}
