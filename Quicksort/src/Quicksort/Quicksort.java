package Quicksort;


public class Quicksort { 

    public static int[] intArr = { 16, 23, 14, 7, 21, 20, 6, 1, 17, 13, 12, 9, 3, 19, 100, 123, 1352,14155,15125,141,41,245,1256,21,67,123456 };
	
    public int[] sort(int l, int r) { 
    	
        int q; 
        if (l < r) { 
            q = partition(l, r); 
            sort(l, q); 
            sort(q + 1, r); 
        } 
        return intArr; 
    } 

    int partition(int l, int r) { 

        int i, j, x = intArr[(l + r) / 2]; 
        i = l - 1; 
        j = r + 1; 
        while (true) { 
            do { 
                i++; 
            } while (intArr[i] < x); 

            do { 
                j--; 
            } while (intArr[j] > x); 

            if (i < j) { 
                int k = intArr[i]; 
                intArr[i] = intArr[j]; 
                intArr[j] = k; 
            } else { 
                return j; 
            } 
        } 
    } 

    public static void main(String[] args) { 
 //   	double tmp;
 //   		tmp 100000+math.random();
 //    		tmp = Math.floor(tmp)
 //   	}
    		
    	
    	
    	
     Stoppuhr uhr1 = new Stoppuhr();
		uhr1.start();
        Quicksort qs = new Quicksort(); 
        int[] arr = qs.sort(0, intArr.length - 1); 
        uhr1.stopp();
        for (int i = 0; i < arr.length; i++) { 
        	
            System.out.println(i + 1 + ": " + arr[i]); 
            System.out.println(uhr1.getDauerInMs() + "ms");
        } 
    } 
} 