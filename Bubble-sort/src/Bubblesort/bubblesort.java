package Bubblesort;

//import java.util.Arrays.toString(int[])

public class bubblesort {
	
	public static int[] bubblesort(int[] a) {
		int temp;
		for(int i=1; i<a.length; i++) {
			for(int j = 0; j < a.length-i; j++) {
				if(a[j] > a [j+1]) {
					temp = a[j];
					a[j] = a[j+1];
					a[j+1] = temp;
				}
				
			}
		}
		return a;
	}

public static void main(String[] args) {
	int[] unsortiert={1,5,8,2,7,4};
	int[] sortiert=bubblesort(unsortiert);
	
	for (int i = 0; i<sortiert.length; i++) {
		System.out.print(sortiert[i] + ", ");
	}

}

}
