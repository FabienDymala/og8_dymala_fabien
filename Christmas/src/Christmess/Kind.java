package Christmess;

import java.util.*;

public class Kind{
	
	String Nachname;
	String Vorname;
	String Geburtstag;
	String Wohnort;
	int Bravheitsgrad;
	
	
	
	public Kind(String nachname, String vorname, String geburtstag, String wohnort, int bravheitsgrad) {
		super();
		Nachname = nachname;
		Vorname = vorname;
		Geburtstag = geburtstag;
		Wohnort = wohnort;
		Bravheitsgrad = bravheitsgrad;
	}
	
	
	
	public String getNachname() {
		return Nachname;
	}
	public void setNachname(String nachname) {
		Nachname = nachname;
	}
	public String getVorname() {
		return Vorname;
	}
	public void setVorname(String vorname) {
		Vorname = vorname;
	}
	public String getGeburtstag() {
		return Geburtstag;
	}
	public void setGeburtstag(String geburtstag) {
		Geburtstag = geburtstag;
	}
	public String getWohnort() {
		return Wohnort;
	}
	public void setWohnort(String wohnort) {
		Wohnort = wohnort;
	}
	public int getBravheitsgrad() {
		return Bravheitsgrad;
	}
	public void setBravheitsgrad(int bravheitsgrad) {
		Bravheitsgrad = bravheitsgrad;
	} 

}
