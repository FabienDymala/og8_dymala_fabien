package DateiVerwaltung;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.IOException;

public class DateiVerwaltung {

	private File file;

	public DateiVerwaltung(File file) {
		this.file = file;
	}

	public void schreibe(String s) {
		try {
			FileWriter fw =new FileWriter(this.file,true);
				BufferedWriter bw = new BufferedWriter(fw);
				bw.newLine();
				bw.write(s);
				bw.flush();
				bw.close();
			}catch (IOException e) {
				System.out.println("file wahrscheinlich nicht da");
				e.printStackTrace();
			}
		}
	
	public void lesen() {
		try {
			FileReader fr = new FileReader(this.file); 
				BufferedReader br = new BufferedReader(fr);
				String s;
				while((s=br.readLine())!=null) {
					System.out.println(s);
				}
				br.close();
			} catch (Exception e) {
				System.out.print("file wahrscheinlich nich da");
				e.printStackTrace();
			}
	}

	public static void main(String[] args) {
		File file = new File("OG8.txt");
		DateiVerwaltung dv = new DateiVerwaltung(file);
		dv.schreibe("s");
		dv.lesen();
	}
}
