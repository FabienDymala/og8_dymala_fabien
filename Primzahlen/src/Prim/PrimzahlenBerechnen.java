//zusammenarbeit mit Alexader Holz

package Prim;

import java.util.Scanner;

public class PrimzahlenBerechnen {
	
    private static Scanner scan; 
    private static boolean isPrimzahl;
    
	public static void main(String[] args) {
	    System.out.println("bitte Zahl eingeben");
		Scanner scan = new Scanner(System.in);
		long zahl = scan.nextLong();
		boolean isPrimzahl = true ; 
		
		int n = 0;
		
		for (long i = 1; i <= zahl; i++) {
		    if (zahl % i == 0) {
		        n++;
		        if (n >= 3){
		            isPrimzahl = false;
		            break;
		        }
		    }
		}
		if (isPrimzahl) {
		    System.out.println(zahl + " ist eine Primzahl.");
		}
		else {
		    System.out.println(zahl + " ist keine Primzahl.");
		}	
}
}