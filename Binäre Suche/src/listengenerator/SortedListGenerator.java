package listengenerator;

import java.util.Random;

public class SortedListGenerator {

	public static long[] getSortedList(int laenge){
		Random rand = new Random(111111);
		long[] zahlenliste = new long[laenge];
		long naechsteZahl = 0;
		
		for(int i = 0; i < laenge; i++){
			naechsteZahl += rand.nextInt(3)+1;
			zahlenliste[i] = naechsteZahl;
		}
		
		return zahlenliste;
	}
	
	static void binaerSuche(int[] feld, int links,int rechts, int kandidat) {
        int mitte;
    do{
        System.out.println("Intervall [" + links +
                "," + rechts + "]");
        
            mitte = (rechts + links) / 2;
            if(feld[mitte] < kandidat){
                links = mitte + 1;
            } else {
                rechts = mitte - 1;
            }
     } while(feld[mitte] != kandidat && links <= rechts);
     if(feld[mitte]== kandidat){
            System.out.println("Position: " + mitte);
        } else {
            System.out.println("Wert nicht vorhanden!");
        }
    }


	
	public static void main(String[] args){
		final int ANZAHL = 20000000; //20.000.000
		
		long[] a = getSortedList(ANZAHL);
		long gesuchteZahl = a[ANZAHL-1];
		
		long time = System.currentTimeMillis();
		
		for(int i = 0; i < a.length; i++)
			if(a[i] == gesuchteZahl)
				System.out.println(i);
		
		System.out.println(System.currentTimeMillis() - time + "ms");
		
		//for(long x: a)
		//	System.out.println(x);	
		int groesse=20000000;
        int[] feld = new int[groesse];
        long time1 = System.currentTimeMillis();
        
        for (int i=0; i<feld.length;i++)
            feld[i] = 2*i; //Feld besteht aus geraden Zahlen
        System.out.println("Suche feld["+ 66 + "]=" + feld[66]);
        binaerSuche(feld, 0,(feld.length-1), feld[66]);
        
		System.out.println(System.currentTimeMillis() - time1 + "ms");
    }

	}
