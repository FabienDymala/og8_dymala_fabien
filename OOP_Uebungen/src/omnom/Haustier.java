package omnom;

public class Haustier {
  
	private int hunger;
	private int muede;
	private int zufrieden;
	private int gesund;
	private String name;
	
	public Haustier() {
	this.hunger =100;
	this.muede=100;
	this.zufrieden=100;
	this.gesund=100;	
	}
	
	public Haustier (String name) {
		this.name = name;
		this.hunger =100;
		this.muede=100;
		this.zufrieden=100;
		this.gesund=100;
	}

		public int getHunger() {
			return hunger;
		}
		
		public void setHunger(int hunger) {
			this.hunger = hunger;
			
			if (hunger >100 || hunger<0 ) {
				System.out.println("wert nicht im angabebereich");				
			}else {
				this.hunger = hunger;
			}
		}

		public int getMuede() {
			return muede;
		}

		public void setMuede(int muede) {
			this.muede = muede;
			
			if (muede > 100 || muede<0 ) {
				System.out.println("wert nicht im angabebereich");				
			}else {
				this.muede = muede;
			}
		}
	

		public int getZufrieden() {
			return zufrieden;
		}

		public void setZufrieden(int zufrieden) {
			this.zufrieden = zufrieden;
			
			if (zufrieden > 100 || zufrieden<0 ) {
				System.out.println("wert nicht im angabebereich");				
			}else {
				this.zufrieden = zufrieden;
			}
		}

		public int getGesund() {
			return gesund;
		}

		public void setGesund(int gesund) {
			this.gesund = gesund;
			
			if (gesund > 100 || gesund<0 ) {
				System.out.println("wert nicht im angabebereich");
			}else {
				this.gesund = gesund;
			}
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	
		public void fuettern(int anz) {
			
		  hunger = hunger + anz;
		  
		  if (hunger > 100 || hunger<0 ) {
				System.out.println("wert nicht im angabebereich");				
			}
		}
		
		public void schlafen(int dauer) {
		 
			muede = muede + dauer;
			
			if (muede > 100 || muede<0 ) {
				System.out.println("wert nicht im angabebereich");				
			}			
		}
	
		public void spielen(int dauer) {
		
			zufrieden = zufrieden + dauer;
			
			if (zufrieden >100 || zufrieden<0 ) {
				System.out.println("wert nicht im angabebereich");
			}		
		}
	
		public void heilen() {
			
         gesund =100;
		
		}
}
	

